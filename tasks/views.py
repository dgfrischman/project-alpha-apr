from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskForm
from tasks.models import Task


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form = form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "tasks": tasks,
    }
    return render(request, "tasks/mine.html", context)

@login_required
def task_update(request, id):
  model_instance = Task.objects.get(id=id)
  if request.method == "POST":
    form = TaskForm(request.POST, instance=model_instance)
    if form.is_valid():
      task = form.save()
      return redirect("task_update", id=task.id)
  else:
    form = TaskForm(instance=model_instance)

  context = {
    "form": form
  }

  return render(request, "tasks/edit.html", context)
