from typing import Any
from django.db.models.query import QuerySet
from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm
from django.views.generic import ListView


@login_required
def list_projects(request):
    project = Project.objects.filter(owner=request.user)
    context = {"project": project}

    return render(request, "projects/projects.html", context)


@login_required
def show_project(request, id):
    show_project = Project.objects.get(id=id)
    context = {"show_project": show_project}
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.purchaser = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
